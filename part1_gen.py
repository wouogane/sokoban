# -*- coding: utf-8 -*-
"""
Created on Mon May 21 03:12:01 2018

@author: Nelson
"""

import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel
from gen_plateau import Ui_generateur_de_tableau
from sokoban_joueur import Joueur
#from applicationIHM import SokoIhm


class Fenetre_prin(QWidget):
 
    def __init__(self,fenetre_principale):
        super().__init__()
        # Configuration de l'interface utilisateur.
        self.ui = Ui_generateur_de_tableau()
        self.ui.setupUi(self)
        self.fene_principal=fenetre_principale
    
#        self.sok = SokoIhm()
        self.ui.pushButton.clicked.connect(self.lancer_jeu)
        
        ### associe à chaque spinbox et comboBox une valeur
#        self.nom_niveau = {0:'essai.txt', 1:'essai1.txt', 2:'essai2.txt', 3:'nivo1.txt'}
#        self.liste_aides = {"Je ne veux pas d\'aide":'0', "Caisse Blocage":'1', "Chemin_direct":'2', "Blocage et chemin direct":'3'}
        
        ## fonction permettant de lancer la fenetre principale
    def lancer_jeu(self):
        print(self.ui.spinBox.value())
        print(self.ui.comboBox.view())
        print('essai'+str(self.ui.spinBox.value())+'.txt')
        aide=str(self.ui.comboBox.currentIndex())
        niveaux = 'essai'+str(self.ui.spinBox.value())+'.txt'

        
        self.hide()
        
        self.fene_principal.joueur=Joueur(niveaux, aide)
        self.fene_principal.show()
        
#        self.SokoIhm.show()
#        self.ui.centralwidget.update()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Fenetre_prin()
    ex.show()
    sys.exit(app.exec_())
