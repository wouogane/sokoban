
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 25 09:37:53 2018

@author: teysseau
"""
from plateau_jeu import Plateau_jeu


class Aides():
    
    def __init__(self):
        self.openlist = []
        self.closedlist = []
        
class Blocage(Aides): 
    """Aide indiquant au joueur les caisses C tel qu'il existe un mouvement qui peut bloquer la caisse C, dans le sens où elle ne pourra plus bouger car elle sera coincée entre deux murs)"""
      
    def detecter(self, P):
        caisses_blocables = []
        for C in P.caisse:
            x = C[0]
            y = C[1]
            
            if ([x-1, y-1] in P.mur and [x, y-2] in P.mur and [x, y-1] not in P.mur and [x, y-1] not in P.caisse
                or [x, y-2] in P.mur and [x+1, y-1] in P.mur and [x, y-1] not in P.mur and [x, y-1] not in P.caisse
                or [x+1, y-1] in P.mur and [x+2, y] in P.mur and [x+1, y] not in P.mur and [x+1, y] not in P.caisse
                or [x+2, y] in P.mur and [x+1, y+1] in P.mur and [x+1, y] not in P.mur and [x+1, y] not in P.caisse
                or [x+1, y+1] in P.mur and [x, y+2] in P.mur and [x, y+1] not in P.mur and [x, y+1] not in P.caisse
                or [x, y+2] in P.mur and [x-1, y+1] in P.mur and [x, y+1] not in P.mur and [x, y+1] not in P.caisse
                or [x-1, y+1] in P.mur and [x-2, y] in P.mur and [x-1, y] not in P.mur and [x-1, y] not in P.caisse
                or [x-2, y] in P.mur and [x-1, y-1] in P.mur and [x-1, y] not in P.mur and [x-1, y] not in P.caisse):
                caisses_blocables.append(C)
#        if self.L == []:
#            print('Aucune caisse immédiatement blocable')
        print('Les caisses immédiatement blocables sont :', caisses_blocables)
            
        
        
    
#            
class CheminDirect(Aides):
    """Aide indiquant au joueur les caisses atteignables directement c'est à dire sans bouger d'autres caisses"""
  
    
    def detecter(self, P):
        caisses_atteignables = []
        for caisse in P.caisse:
            x, y = P.personnage[0][0], P.personnage[0][1]
            X, Y = caisse[0], caisse[1]
            if self.existence_chemin_recursif(P, x, y, X, Y) == True:
                caisses_atteignables.append([X, Y])
        print('Les caisses atteignables directement sont :', caisses_atteignables)
            
    
    def existence_chemin(self, P, abc_caisse, ord_caisse):

            openlist = []
            closedlist = []
           
            coord_courante = [P.personnage[0][0], P.personnage[0][1]]     #position joueur
            X, Y = abc_caisse, ord_caisse
            #cout = distance a vol d'oiseau de la caisse destination. Le choix l'heurisitque utilisée pour calculer le cout détermine la convergence optimale ou non de l'algorithme (pas nécessaire ici, on cherche simplement à savoir s'il existe un chemin, pas le meilleur)
            #distance = (distance de la case au chemin -> toujours la même!! +) distance de la case à la caisse destination
            while not coord_courante == [X, Y]: #s'arrêter quand le noeud courant est le noeud final (caisse à atteindre)
                x, y = coord_courante[0], coord_courante[1]

                d1 = abs(X - x) + abs(Y - (y+1))
                d2 = abs(X - x) + abs(Y - (y-1))
                d3 = abs(X - (x+1)) + abs(Y - y)
                d4 = abs(X - (x-1)) + abs(Y - y)
                case_adj1 = [[x, y+1], [x, y], d1]
                case_adj2 = [[x, y-1], [x, y], d2]
                case_adj3 = [[x+1, y], [x, y], d3]
                case_adj4 = [[x-1, y], [x, y], d4]           
                case_adj = [case_adj1, case_adj2, case_adj3, case_adj4]
                
                for case in case_adj:
                    if case[0] == [X, Y]:
                        return True
                    elif case[0] in P.mur or case[0] in P.caisse:
                        pass
                    elif case[0] in closedlist:
                        pass
                    
                    elif case[0] in [openlist[i] for i in range(len(openlist))]:
                        mettre_a_jour(case, openlist)

                    else:
                        openlist.append(case)   #on ajoute dans openlist avec parent = noeud courant
                if openlist == []:
                    return False
                
                else:
                    distance = 100000
                    meilleure_case = []
                    for case in openlist:       #chercher meilleur noeud openlist
                        if case[2] < distance:
                            distance = case[2]
                            meilleure_case = case
                    closedlist.append(meilleure_case[0]) #mettre noeud dans closedlist
                    openlist.remove(meilleure_case)  # l'enlever de openlist
                    coord_courante = meilleure_case[0]  #recommencer avec comme noeud courant le dernier noeud de la closedlist
            
            return True
        
        

    def existence_chemin_recursif(self, P, x, y, X, Y):

         if [x, y] == [X, Y]:
             return True
         
         d1 = abs(X - x) + abs(Y - (y+1))
         d2 = abs(X - x) + abs(Y - (y-1))
         d3 = abs(X - (x+1)) + abs(Y - y)
         d4 = abs(X - (x-1)) + abs(Y - y)
         case_adj1 = [[x, y+1], [x, y], d1]
         case_adj2 = [[x, y-1], [x, y], d2]
         case_adj3 = [[x+1, y], [x, y], d3]
         case_adj4 = [[x-1, y], [x, y], d4]           
         case_adj = [case_adj1, case_adj2, case_adj3, case_adj4]
         
         for case in case_adj:
             if case[0] == [X, Y]:
                 return True
             elif case[0] in P.mur or case[0] in P.caisse:
                 pass
             elif case[0] in self.closedlist:
                 pass
            
             elif case[0] in [self.openlist[i] for i in range(len(self.openlist))]:
                 mettre_a_jour(case, self.openlist)

             else:
                 self.openlist.append(case)   #on ajoute dans openlist avec parent = noeud courant
         if self.openlist == []:
             return False
        
         else:
             distance = 100000
             meilleure_case = []
             for case in self.openlist:       #chercher meilleur noeud openlist
                 if case[2] < distance:
                     distance = case[2]
                     meilleure_case = case
             self.closedlist.append(meilleure_case[0]) #mettre noeud dans closedlist
             self.openlist.remove(meilleure_case)  # l'enlever de openlist
           
             x, y = meilleure_case[0][0], meilleure_case[0][1]
             
         return self.existence_chemin_recursif(P, x, y, X, Y)
         

                
def mettre_a_jour(x, L): 
    for i in range(len(L)):
        if x[0] in L[i] and x[2] < L[i][2]:
            L.remove(L[i])
            L.append(x)