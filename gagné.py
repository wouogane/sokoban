# -*- coding: utf-8 -*-
"""
Created on Mon May 21 01:18:43 2018

@author: Nelson
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 20 18:00:58 2018

@author: wouogane
"""

import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLabel
from PyQt5.QtGui import QIcon
from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import pyqtSlot
from sokoban_joueur import Joueur
 
class App_win(QWidget):
 
    def __init__(self):
        super().__init__()
        self.title = 'gagner'
        self.left = 100 #50
        self.top = 100 #50
        self.width = 650 #320
        self.height = 200 #200
        self.initUI()
        self.joueur = Joueur('essai1.txt', '1')
                
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("sprites_mario_sokoban/superMario_sokoban.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)
        palette = QtGui.QPalette()
        pixmap = QtGui.QPixmap("fond.jpg")
        palette.setBrush(QtGui.QPalette.Background,QtGui.QBrush(pixmap))
        self.setPalette(palette)
        
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
 
        self.affiche = QLabel("<font color='red' size='11'>Vous avez gagné</font>", self)
        self.affiche.setToolTip('gagné')
        self.affiche.move(30,80)
        self.affiche.resize(800,50)
#        affiche.clicked.connect(self.aide_joueur)
 
        self.show()
 
    @pyqtSlot()
    def gagne(self):
        print('vous avez gagné')
        self.joueur.a_gagne()
        self.ui.centralwidget.update()
     
 
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App_win()
    sys.exit(app.exec_())