# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 17:24:36 2018

@author: Auxane
"""
import numpy as np

def affiche(P):
    """Affiche un plateau de jeu sous la forme d'un array.
    
    Parametre
    ---------
    P : objet de la classe Plateau_jeu
    
    Renvoie
    -------
    Rien
    """
    plateau = []
    for i in range(P.taille[0]):
        plateau.append([0]*P.taille[1])  
       
    plateau[int(P.personnage[0][0]-1/2)][int(P.personnage[0][1]-1/2)] = '@'
    for x in P.mur:
        plateau[int(x[0]-1/2)][int(x[1]-1/2)] = '#'
    for x in P.caisse:
        plateau[int(x[0]-1/2)][int(x[1]-1/2)] = '$'
    for x in P.goal:
        if x in P.caisse:
            plateau[int(x[0]-1/2)][int(x[1]-1/2)] = '*'
        elif x in P.personnage:
            plateau[int(x[0]-1/2)][int(x[1]-1/2)] = '+'
        else:
            plateau[int(x[0]-1/2)][int(x[1]-1/2)] = '.'
   
    for i in range(P.taille[0]):
        for j in range(P.taille[1]):
            if plateau[i][j] == 0:
                plateau[i][j] = ' '
    print(np.asarray(plateau))
    return plateau
   