# -*- coding: utf-8 -*-
"""
Created on Mon May 21 03:43:35 2018

@author: Nelson
"""
import sys
from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton
from sokoban_joueur import Joueur
from applicationIHM import SokoIhm


class Lancer_jeu(QWidget):
    
    def __init__(self):
        
        super().__init__()
#        self.ui = Fenetre_prin()
        self.ui.setupUi(self)
            
        self.sok = SokoIhm()
        
        self.ui.pushButton.clicked.connect(self.lancer_jeu)
        
        ### associe à chaque spinbox et comboBox une valeur
        self.QSpinBox.nom_niveau = {0:'essai.txt', 1:'essai1.txt', 2:'essai2.txt', 3:'nivo1.txt'}
        self.QComboBox.liste_aides = {"Je ne veux pas d\'aide":'0', "Caisse Blocage":'1', "Chemin_direct":'2', "Blocage et chemin direct":'3'}
        
        ## fonction permettant de lancer la fenetre principale
        def lancer_jeu(self):
            self.lancer = Joueur(self.QSpinBox.nom_niveau, self.QComboBox.liste_aides)
            self.sok.show()
        

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Lancer_jeu()
    ex.show()
    sys.exit(app.exec_())
    