# -*- coding: utf-8 -*-
"""
Created on Wed May 16 13:51:16 2018

@author: wouogane
"""

import sys
from PyQt5 import QtGui, QtCore, QtWidgets
#from PyQt5.QtWidgets import QLabel
from sokoban import Ui_principale_ihm
from sokoban_joueur import Joueur
from plateau_jeu import Plateau_jeu
from gagné import App_win
from part1_gen import Fenetre_prin


class SokoIhm(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        # Configuration de l'interface utilisateur.
        self.ui = Ui_principale_ihm()
        self.ui.setupUi(self)
        # TO DO
        palette = QtGui.QPalette()
        pixmap = QtGui.QPixmap("fond.jpg")
        palette.setBrush(QtGui.QPalette.Background,QtGui.QBrush(pixmap))
        self.setPalette(palette)
        
        #### affiche le plateau de jeu
        self.joueur = None
        self.gen = Fenetre_prin(self)
        self.gagner = App_win()
        
        
#        self.ui.pushButton.clicked.connect(self.lancer_jeu)
#        
#        ### associe à chaque spinbox et comboBox une valeur
#        self.gen.QSpinBox.nom_niveau = {0:'essai.txt', 1:'essai1.txt', 2:'essai2.txt', 3:'nivo1.txt'}
#        self.gen.QComboBox.liste_aides = {"Je ne veux pas d\'aide":'0', "Caisse Blocage":'1', "Chemin_direct":'2', "Blocage et chemin direct":'3'}
        # on cache  la fenetre  gagner
        self.gagner.hide()
        # cache la fenetre principale
        self.hide()
        
        self.gen.show()  


        self.ui.centralwidget.update()
        
        #### Connexion entre les boutons et les touches de jeu
        
        self.ui.bouton_gauche.clicked.connect(self.Gauche)
        self.ui.bouton_droit.clicked.connect(self.Droite)
        self.ui.bouton_av.clicked.connect(self.Haut)
        self.ui.bouton_recul.clicked.connect(self.Bas)
        
#        self.ui.pushButton.clicked.connect(self.lancer_jeu)
        
#        self.ui.bouton_ret_ar.clicked.connect(self.bouton_ret_ar)
#        self.ui.bouton_reinit.clicked.connect(self.bouton_reinit)
        
#        self.timer = QtCore.QTimer()
#        self.timer.timeout.connect(self.plateau.evolue("left"))
#        self.timer.timeout.connect(self.plateau.evolue("right"))
#        self.timer.timeout.connect(self.plateau.evolue("up"))
#        self.timer.timeout.connect(self.plateau.evolue("down"))
        
            # démarrage du timer qui servira à raffraichir l'affichage du GUI
#    def updateTimer(self):
#        self.timer.setInterval(100)
#        self.timer.start()

#    def lancer_jeu(self):
#        
#        print(self.ui.spinBox.value())
#        print(self.ui.comboBox.view())
#        print('essai'+str(self.ui.spinBox.value())+'.txt')
#        aide=str(self.ui.comboBox.currentIndex())
#        niveaux = 'essai'+str(self.ui.spinBox.value())+'.txt'
#        self.joueur = Joueur(niveaux, aide) 
        
    def Haut(self) :
        print("Up")
        self.joueur.P.evolue("up") 
        self.joueur.compteur +=1 
        self.gagne_partie()
        self.ui.centralwidget.update() # nécessaire pour la MAJ de l’IHM

    def Bas(self) :
        print("Down")
        self.joueur.P.evolue("down")
        self.joueur.compteur +=1 
        self.gagne_partie()
        self.ui.centralwidget.update()
    
    def Gauche(self):
        print("Left")
        self.joueur.P.evolue("left")
        self.joueur.compteur +=1 
        self.gagne_partie()
        self.ui.centralwidget.update()
        
    def Droite(self):
        print("Right")
        self.joueur.P.evolue("right")
        self.joueur.compteur +=1 
        self.gagne_partie()
        self.ui.centralwidget.update()
                
    def paintEvent(self,e) :
        
        p=QtGui.QPainter(self.ui.conteneur)
        p.begin(self)
        
        x_orig,y_orig=20,10
        
        width,height=900,550
        
        p.fillRect(x_orig,y_orig,width,height,QtCore.Qt.white)

        if self.joueur:
            self.joueur.P.dessinplateau(p)        
        p.end()  
        
    def gagne_partie(self):
        
        if self.joueur.a_gagne(): 
            print('whaou!')
            self.gagner.affiche.setText("<font color='blue' size='9'>Vous avez gagné! Nombre de déplacements est :</font>"+ str(self.joueur.compteur)) 
            self.gagner.show()
                        

        
#        ## fonction permettant de lancer la fenetre principale
#        def lancer_jeu(self):
#            self.lancer = Joueur(self.QSpinBox.nom_niveau, self.QComboBox.liste_aides)        
#            self.affiche = QLabel(str(self.QComboBox.liste_aides), self)
#            self.affiche.setToolTip('aide')
#            self.affiche.move(30,80)
#            self.affiche.resize(800,50)
#            self.sokoIhm.show()
        
        
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = SokoIhm()
#    window.show()
    app.exec_()
