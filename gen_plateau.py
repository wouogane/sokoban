# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gen_plateau.ui'
#
# Created by: PyQt5 UI code generator 5.8.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_generateur_de_tableau(object):
    def setupUi(self, generateur_de_tableau):
        generateur_de_tableau.setObjectName("generateur_de_tableau")
        generateur_de_tableau.resize(400, 300)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("sprites_mario_sokoban/superMario_sokoban.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        generateur_de_tableau.setWindowIcon(icon)
        generateur_de_tableau.setWindowOpacity(1.0)
        generateur_de_tableau.setStyleSheet("background-color: rgb(252, 233, 79);")
        self.spinBox = QtWidgets.QSpinBox(generateur_de_tableau)
        self.spinBox.setGeometry(QtCore.QRect(20, 130, 61, 31))
        font = QtGui.QFont()
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.spinBox.setFont(font)
        self.spinBox.setStyleSheet("background-color: rgb(245, 121, 0);")
        self.spinBox.setObjectName("spinBox")
        self.comboBox = QtWidgets.QComboBox(generateur_de_tableau)
        self.comboBox.setGeometry(QtCore.QRect(178, 130, 191, 41))
        font = QtGui.QFont()
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.comboBox.setFont(font)
        self.comboBox.setStyleSheet("background-color: rgb(245, 121, 0);")
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.label = QtWidgets.QLabel(generateur_de_tableau)
        self.label.setGeometry(QtCore.QRect(0, 50, 151, 31))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setStyleSheet("background-color: rgb(78, 154, 6);")
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(generateur_de_tableau)
        self.label_2.setGeometry(QtCore.QRect(220, 50, 131, 31))
        font = QtGui.QFont()
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet("background-color: rgb(78, 154, 6);")
        self.label_2.setObjectName("label_2")
        self.pushButton = QtWidgets.QPushButton(generateur_de_tableau)
        self.pushButton.setGeometry(QtCore.QRect(110, 220, 101, 31))
        font = QtGui.QFont()
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.pushButton.setFont(font)
        self.pushButton.setStyleSheet("background-color: rgb(245, 121, 0);")
        self.pushButton.setObjectName("pushButton")

        self.retranslateUi(generateur_de_tableau)
        QtCore.QMetaObject.connectSlotsByName(generateur_de_tableau)

    def retranslateUi(self, generateur_de_tableau):
        _translate = QtCore.QCoreApplication.translate
        generateur_de_tableau.setWindowTitle(_translate("generateur_de_tableau", "Generateur"))
        self.comboBox.setItemText(0, _translate("generateur_de_tableau", "Je ne veux pas d\'aide"))
        self.comboBox.setItemText(1, _translate("generateur_de_tableau", "Caisse Blocage"))
        self.comboBox.setItemText(2, _translate("generateur_de_tableau", "Chemin_direct"))
        self.comboBox.setItemText(3, _translate("generateur_de_tableau", "Blocage et chemin direct"))
        self.label.setText(_translate("generateur_de_tableau", "Sélection des niveaux"))
        self.label_2.setText(_translate("generateur_de_tableau", "Sélection des aides"))
        self.pushButton.setText(_translate("generateur_de_tableau", "Jouer"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    generateur_de_tableau = QtWidgets.QWidget()
    ui = Ui_generateur_de_tableau()
    ui.setupUi(generateur_de_tableau)
    generateur_de_tableau.show()
    sys.exit(app.exec_())

