# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 17:10:26 2018

@author: Nelson
"""
from plateau_jeu import Plateau_jeu
"""Ce module contient la classe Plateau_jeu gérant le jeu
"""
from sokoban_affiche import affiche
"""Ce module contient la fonction affiche qui gère l'affichage du plateau
"""
from sokoban_aides import Blocage, CheminDirect


class Joueur():
    
    def __init__(self, nom_niveau, liste_aides):
        self.P = Plateau_jeu()
        self.charge_niveau(nom_niveau)
        self.compteur = 0
        self.aides = liste_aides
        
    def deplacer(self,  direction):

        """Execute l'evolution adequate du plateau de jeu 
        en fonction de la commande de l'utilisateur
        
        Parametre
        ---------
        direction : str
            instruction du joueur
            
        Renvoie
        -------
        Rien
        """
        if direction == "u":
             self.P.evolue("up")
             return True
        elif direction == "d":
             self.P.evolue("down")
             return True
        elif direction == "l":
             self.P.evolue("left")
             return True
        elif direction == "r":
             self.P.evolue("right")
             return True
        elif direction == "exit":
             print("Perdu par forfait !")
             return None 
        else:
             print("Commande non reconnue")
             return False
             
    def charge_niveau(self, nom_niveau): #permet de recharger un niveau sans recréer un joueur (recommmencer une partie) 
                                            #et garder l'historique des scores du joueur (faire le lien avec le compteur)
        self.P.charge_niveau(nom_niveau)
        affiche(self.P)
        
    def a_gagne(self):
         self.P.caisse.sort
         self.P.goal.sort
         if self.P.caisse == self.P.goal:
             return True
         else:
             return False
         
    def aider(self):
        for A in self.aides:
            print(A.detecter(J.P))
            
    
            
         

#        """ Effectue toutes les actions liées à une partie de Sokoban :
#            chargement du niveau, deplacements du joueur et affichage du plateau
#       
#        Parametre
#        ---------
#        nom_niveau : str
#            nom du niveau à jouer sous la forme "nom_niveau.txt"
#            
#        Renvoie 
#        -------
#        Rien
#        """
#        


if __name__ == '__main__':
    niveau = input('Nom du niveau ? ')
    print(niveau)
    J = Joueur(niveau, [])
    bl = Blocage()
    cd = CheminDirect()
    choix_aides = input('De quelles aides souhaitez-vous bénéficier ? Tapez 0 pour aucune aide, 1 pour Blocage, 2 pour CheminDirect ou 3 pour Blocage et CheminDirect ')
    if choix_aides == '0':
        pass
    elif choix_aides == '1':
        J.aides.append(bl)
    elif choix_aides == '2':
        J.aides.append(bl)
    elif choix_aides == '3':
        J.aides.append(bl)
        J.aides.append(cd)

    c = J.compteur
    direction = 0
    while not J.a_gagne() and not J.deplacer(direction) == None:
        J.aider()
        direction = input('Instruction du joueur ?')
        print(direction) 
#        if direction == "reset":
#            print(niveau)
#            reponse = input('Voulez-vous vraiment recommencer le niveau ?')
#            if reponse == "oui":
#                c = 0
#                J.charge_niveau(niveau)         
#        else:
        c += 1
        J.deplacer(direction)
        affiche(J.P)
    if J.a_gagne(): #== True:
        print("Vous avez gagné! Nombre de déplacements :", c)
    
    #recharge le niveau
#    J.charge_niveau(niveau)
    