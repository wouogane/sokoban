#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 20 18:00:58 2018

@author: wouogane
"""

import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton
from PyQt5.QtGui import QIcon
from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import pyqtSlot
from sokoban_joueur import Joueur
 
class App(QWidget):
 
    def __init__(self):
        super().__init__()
        self.title = 'Aides joueurs'
        self.left = 10
        self.top = 10
        self.width = 320
        self.height = 200
        self.initUI()
        self.joueur = Joueur('essai2.txt', '0')
                
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("sprites_mario_sokoban/superMario_sokoban.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)
        palette = QtGui.QPalette()
        pixmap = QtGui.QPixmap("fond_aide.jpg")
        palette.setBrush(QtGui.QPalette.Background,QtGui.QBrush(pixmap))
        self.setPalette(palette)
        
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
 
        button = QPushButton('Aides', self)
        button.setToolTip('Aides')
        button.move(100,70) 
        button.clicked.connect(self.aide_joueur)
 
        self.show()
 
    @pyqtSlot()
    def aide_joueur(self):
        print('''De quelles aides souhaitez-vous bénéficier ? 
              Tapez 
              0 pour aucune aide; 
              1 pour Blocage; 
              2 pour CheminDirect; 
              3 pour Blocage et CheminDirect ''')
        self.joueur.aider()
        self.ui.centralwidget.update()
     
 
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())