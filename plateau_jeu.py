# -*- coding: utf-8 -*-
"""
Created on Thu Mar  8 18:24:01 2018

@author: Auxane
"""

import numpy as np
from PyQt5 import QtGui, QtCore, QtWidgets

class Plateau_jeu():
    """Classe gérant le plateau de jeu par stockage des coordonnées des objets 
    dans des listes 
    """
    def __init__(self):
        """Initialisation des variables d'instance à des listes vides
        """
        self.caisse = []
        self.mur = []
        self.goal = []
        self.personnage = []
        self.direction_personnage='down'
        self.taille = [0,0]

        
        
    def charge_niveau(self, nom_niveau):
        """Stocke les coordonnées de chaque objet rencontré dans le niveau 
        dans la liste (variable d'instance) correspondante.
        
        Paramètres
        ----------
        nom_niveau : str
            nom du fichier xsb, sous la forme 'nom_niveau.txt'
            
        Renvoie
        -------
        Rien
        """
        f = open(nom_niveau, 'r')
        plateau = f.readlines()
        f.close()
        print(np.asarray(plateau))
        
        self.taille[0] = len(plateau)
        nbcol_max = 0
        for x in plateau:
            if len(x) > nbcol_max:
                nbcol_max = len(x)
        self.taille[1] = nbcol_max
        
        for i in range(len(plateau)):
            for j in range(len(plateau[i])):
                if plateau[i][j] == '#':    
                    m, n = i+1/2, j+1/2
                    self.mur.append([m,n])
                    
                elif plateau[i][j] == '$':
                    c, d = i+1/2, j+1/2
                    self.caisse.append([c,d])
                    
                elif plateau[i][j] == '.':
                    g, h = i+1/2, j+1/2
                    self.goal.append([g,h])
                    
                elif plateau[i][j] == '@':
                    k, l = i+1/2, j+1/2
                    self.personnage.append([k,l])
                    
                elif plateau[i][j] == '*':
                    c, d = i+1/2, j+1/2
                    self.caisse.append([c,d])
                    g, h = i+1/2, j+1/2
                    self.goal.append([g,h])
                    
                elif plateau[i][j] == '+':
                    k, l = i+1/2, j+1/2
                    self.personnage.append([k,l])
                    g, h = i+1/2, j+1/2
                    self.goal.append([g,h])
                    

       
           
    def evolue(self, deplacement):
        """Methode qui met à jour les listes de coordonnées des caisses et du joueur 
        après déplacement du joueur
        
        Paramètre
        ---------
        deplacement : str
            est relié à l'instruction du joueur
            
        Renvoie
        -------
        Rien
        """
        self.direction_personnage=deplacement
        
        case = [self.personnage[0][0], self.personnage[0][1]]
        if deplacement == "left":
            case_adjacente = [self.personnage[0][0], self.personnage[0][1]-1]
            case_adjacente2 = [self.personnage[0][0], self.personnage[0][1]-2]
        elif deplacement == "right":
            case_adjacente = [self.personnage[0][0], self.personnage[0][1]+1]
            case_adjacente2 = [self.personnage[0][0], self.personnage[0][1]+2]
        elif deplacement == "up":
            case_adjacente = [self.personnage[0][0]-1, self.personnage[0][1]]
            case_adjacente2 = [self.personnage[0][0]-2, self.personnage[0][1]]
        elif deplacement == "down":
            case_adjacente = [self.personnage[0][0]+1, self.personnage[0][1]]
            case_adjacente2 = [self.personnage[0][0]+2, self.personnage[0][1]]
            
            
        if case_adjacente in self.mur:
            print("Ce mouvement est impossible")        
            
        elif case_adjacente in self.caisse:
            if case_adjacente2 in self.mur or case_adjacente2 in self.caisse:
                print("Ce mouvement est impossible")
            else:
                self.caisse.remove(case_adjacente)
                self.caisse.append(case_adjacente2)
                self.personnage.remove(case)
                self.personnage.append(case_adjacente)

                    
        else:
            self.personnage.remove(case)
            self.personnage.append(case_adjacente)
            
            
            
            
    def dessinplateau(self, qp,x_orig=50,y_orig=100 ,width=400,height=500):
                
        
        
        width_objet=width/self.taille[0]
        
        
        height_objet=height/self.taille[1]
        
        chemin_img_caisse='sprites_mario_sokoban/caisse.jpg'
        image_caisse = QtGui.QImage(chemin_img_caisse)
        
        chemin_img_caisse_ok ='sprites_mario_sokoban/caisse_ok.jpg'
        image_caisse_ok = QtGui.QImage(chemin_img_caisse_ok)
        
        chemin_img_mur='sprites_mario_sokoban/mur.jpg'
        image_mur = QtGui.QImage(chemin_img_mur)
        
        chemin_img_goal='sprites_mario_sokoban/objectif.png'
        
        
        image_goal= QtGui.QImage(chemin_img_goal)
        
        
        images_position_personnage={'up':QtGui.QImage("sprites_mario_sokoban/mario_haut.gif"),'left':QtGui.QImage("sprites_mario_sokoban/mario_gauche.gif"),'right':QtGui.QImage("sprites_mario_sokoban/mario_droite.gif"),'down':QtGui.QImage("sprites_mario_sokoban/mario_bas.gif")}
        
        
        
        
        
        ###### affichage personnage
        qp.drawImage(QtCore.QRect(x_orig+self.personnage[0][1]*width_objet,y_orig+self.personnage[0][0]*height_objet,width_objet, height_objet),images_position_personnage[self.direction_personnage]) 


        ###### affichage mur
        for  mur in self.mur:            
            qp.drawImage(QtCore.QRect(x_orig+mur[1]*width_objet,y_orig+mur[0]*height_objet,width_objet, height_objet),image_mur)
         
            
        ###### affichage goal            
        for  goal in self.goal:
                      
            # si pas presence du personnage
            if goal not in self.personnage:
                qp.drawImage(QtCore.QRect(x_orig+goal[1]*width_objet,y_orig+goal[0]*height_objet,width_objet, height_objet),image_goal)


        ###### afichage des caisses
        for caisse in self.caisse:
            
            # affiche caisse ok si dans les goals
            if caisse in self.goal:
                qp.drawImage(QtCore.QRect(x_orig+caisse[1]*width_objet,y_orig+caisse[0]*height_objet,width_objet,height_objet),image_caisse_ok)
           
            # affiche caisse simple si pas les goals
            else:
                qp.drawImage(QtCore.QRect(x_orig+caisse[1]*width_objet,y_orig+caisse[0]*height_objet,width_objet,height_objet),image_caisse)
                    
