# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 20:32:06 2018

@author: Nelson
"""

import unittest
from plateau_jeu import Plateau_jeu
import sokoban_affiche as s_a
from sokoban_joueur import Joueur
import random

class TestPlateau_jeu(unittest.TestCase):
    
    def test_var(self):
        P = Plateau_jeu()
        self.assertEqual(P.caisse, [])
        self.assertEqual(P.mur, [])
        self.assertEqual(P.goal, [])
        self.assertEqual(P.personnage, [])
        self.assertEqual(P.taille, [0,0])
    
    def test_type(self):
        P = Plateau_jeu()
        self.assertIsInstance(P, Plateau_jeu)
        
    def test_charge_niveau(self):
        P = Plateau_jeu()
        P.charge_niveau('essai3.txt')
        
        f = open('essai3.txt', 'r')
        plateau = f.readlines()
        f.close()
        ligne = random.choice(plateau)
        element = random.choice(ligne)
        i = plateau.index(ligne)
        j = ligne.index(element)
        
        if plateau[i][j] == "#":
            self.assertIn([i+1/2, j+1/2], P.mur)
        elif plateau[i][j] == "$":
            self.assertIn([i+1/2, j+1/2], P.caisse)
        elif plateau[i][j] == ".":
            self.assertIn([i+1/2, j+1/2], P.goal)    
        elif plateau[i][j] == "@":
            self.assertIn([i+1/2, j+1/2], P.personnage)
        elif plateau[i][j] == "*":
            self.assertIn([i+1/2, j+1/2], P.caisse)
            self.assertIn([i+1/2, j+1/2], P.goal)
        elif plateau[i][j] == "+":
            self.assertIn([i+1/2, j+1/2], P.personnage)
            self.assertIn([i+1/2, j+1/2], P.goal)
        
    def test_evolue(self):
        P = Plateau_jeu()
        P.charge_niveau("essai2.txt")
        s_a.affiche(P)
        case = [P.personnage[0][0], P.personnage[0][1]]
        case_adjacente = [P.personnage[0][0], P.personnage[0][1]+1]
        P.evolue("right")
        s_a.affiche(P)
        self.assertIn(case_adjacente, P.personnage)
        self.assertNotIn(case, P.personnage)

        
class TestJoueur(unittest.TestCase):
     def test_var(self):
         pass
     
     def test_type(self):
         J = Joueur()
         self.assertIsInstance(J, Joueur)
            
            
class TestAffichage(unittest.TestCase):
    
    def test_affiche(self):
        P = Plateau_jeu()
        plateau = s_a.affiche(P)
        
        m = random.choice(P.mur)
        i = m[0] - 1/2
        j = m[1] - 1/2
        self.assertEqual(plateau[i][j], '#')
        
        
        
if __name__ == "__main__":       
    unittest.main()
        
    
    